範例一
```mermaid
graph LR

A[Hard edge] -->B(Round edge)

    B --> C{Decision}

    C -->|One| D[Result one]

    C -->|Two| E[Result two]

```
<br>
範例二
   ```mermaid
graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;
   ```

