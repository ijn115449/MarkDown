# Summary



# Sequence (只支援Typora)

- [Sequence](Doc/Sequence/Sequence.md)


# Flowchart (只支援Typora)

- [flow](Doc/Flowchart/flow.md)

# Mermaid (GitLab支援)

- [Sequence](Doc/Mermaid/Sequence.md)
- [Flowchart](Doc/Mermaid/Flowchart.md)
- [Gantt](Doc/Mermaid/Gantt.md)

